package Pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AuthPage extends BasePage {

    public AuthPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);

    }

    @FindBy(xpath = "//input[@id='email']")
    public WebElement email;

    @FindBy(xpath = "//input[@id='passwd']")
    public WebElement password;

    @FindBy(xpath = "//button[@id='SubmitLogin']")
    public WebElement loginButton;

    @FindBy(xpath = "//input[@id='email_create']")
    public WebElement emailField;

    @FindBy(xpath = "//button[@id='SubmitCreate']")
    public WebElement registerEmail;

    @FindBy(xpath = "//*[contains(text(),'Ваш аккаунт был создан')]")
    public WebElement successMessage;

    @FindBy(xpath = "//li[text()='Ошибка авторизации.']")
    public WebElement failedLogin;


    @Step
    public AuthPage enterRegistrationEmail(String email) {
        emailField.sendKeys(email);
        return this;
    }

    @Step
    public RegisterPage clickRegistrationButton() {
        registerEmail.click();
        return new RegisterPage(driver);
    }

    @Step
    public AuthPage fillFields(String str) {
        email.sendKeys("valeriya.kalchenko@lazy-ants.de");
        password.sendKeys(str);
        return this;
    }

    @Step
    public PersonalCabinet clickLoginButton() {
        loginButton.click();
        return new PersonalCabinet(driver);
    }

    @Step
    public AuthPage clickToFailedLoginButton() {
        loginButton.click();
        return this;
    }

}

