package Pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductDetails extends BasePage {
    public ProductDetails(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//div[@class='credit creditonline']")
    public WebElement creditOnline;

    @FindBy(xpath = "//span[text()= 'Код  09118']")
    public WebElement meizuTitleCode;

    @FindBy(xpath = "//img[@id='bigpic']")
    private WebElement zoomProduct;

    @FindBy(xpath = "//img[@class='fancybox-image']")
    public WebElement zoomImg;

    @FindBy(xpath = "//a[@class='fancybox-item fancybox-close']")
    public WebElement сloseZoom;

    @FindBy(xpath = "//div[@class= 'product-compare-block compare']")
    public WebElement comparsionFromDetails;

    @FindBy(xpath = "//div[@id='preCreditWindow']")
    public WebElement preCreditWindow;


    @Step
    public ProductDetails zoomProduct() {
        zoomProduct.click();
        return this;
    }

    @Step
    public ProductDetails closeZoom() {
        сloseZoom.click();
        return this;
    }

    @Step
    public ProductDetails creditOnline() {
        creditOnline.click();
        return this;
    }


}
