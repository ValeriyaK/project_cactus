package Pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProductPage extends BasePage {

    public ProductPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//span[text()= 'Код  08298']")
    public WebElement productId;
    @FindBy(xpath = "//input[@id='layered_id_feature_34']")
    public WebElement filter;
    @FindBy(xpath = "//a[@data-rel='layered_id_feature_34']")
    public WebElement choosedFilter;


    @Step
    public ProductGroupPage filter() {
        new Actions(driver).moveToElement(filter).perform();
        filter.click();
        return new ProductGroupPage(driver);
    }
}

