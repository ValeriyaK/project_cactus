package Tests;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class BaseTest {
    WebDriver driver;

    //для поля емейл,чтобы вводились рандомные буквы
    @DataProvider()
    public Object[][] getEmail() {
        Random r = new Random();
        String str = "";
        for (int i = 0; i < 10; i++) {
            str += (char) (r.nextInt(26) + 'a');
        }
        str += "@gmail.com";

        return new Object[][]{
                {str}
        };
    }


    @BeforeMethod
    public void beforeMethod() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("https://cactus.kh.ua/");
    }

    @AfterMethod
    public void afterMethod() {
        if (driver != null) {
            driver.quit();
        }
    }
}
