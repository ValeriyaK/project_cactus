package Tests;

import Pages.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class ProductTest extends BaseTest {

    @Test
    public void productDetails() {
        ProductDetails productDetails = new MainPage(driver)
                .clickOnProduct();
        Assert.assertTrue(productDetails.creditOnline.isDisplayed());
    }

    @Test
    public void productFilter() {
        ProductPage productPage = new Header(driver)
                .clickOnMeizuPhone()
                .checkBox();
        Assert.assertTrue(productPage.choosedFilter.isDisplayed());
    }

    @Test
    public void creditOnline() {
        ProductDetails productDetails = new MainPage(driver)
                .clickOnProduct();
        productDetails.creditOnline.click();
        Assert.assertTrue(productDetails.preCreditWindow.isDisplayed());
    }

    @Test
    public void zoomImgTest() {
        ProductDetails productDetails = new MainPage(driver)
                .clickOnProduct()
                .zoomProduct();
        Assert.assertTrue(productDetails.zoomImg.isDisplayed());
        productDetails.closeZoom();
        List<WebElement> list = driver.findElements(By.xpath("//a[@class='fancybox-item fancybox-close']"));
        Assert.assertTrue(list.size() == 0);
    }


}




