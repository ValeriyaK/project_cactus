package Tests;

import Pages.Header;
import Pages.MainPage;
import Pages.Result;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SearchTest extends BaseTest {

    @Test
    public void positiveSearch() {
        Result result = new Header(driver)
                .positiveSearch("Часы");
        Assert.assertTrue(result.zeroResults.isDisplayed(), "we have a results");
    }

    @Test
    public void negativeSearch() {
        Result result = new Header(driver)
                .negativeSearch("qwertyui");
        Assert.assertTrue(result.nothingFound.isDisplayed());
    }

    @Test
    public void fastSearch() {
        Result result = new Header(driver)
                .fastSearch();
        Assert.assertTrue(result.blockCategories.isDisplayed());
    }

}
